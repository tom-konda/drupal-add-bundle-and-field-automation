const puppeteer = require('puppeteer');
const config = require('./config.json');
const neatcsv = require('neat-csv');
const fs = require('fs');

async function init() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(`${config.server.drupalURL}/user/login`);
  await page.type('#edit-name', config.admin.user);
  await page.type('#edit-pass', config.admin.password);
  await page.click('#user-login-form #edit-submit');
  await page.waitForSelector('#toolbar-item-user');
  let result = fs.readFileSync('./bundle.csv');
  let data = await neatcsv(result, {
    separator: "\t",
    newline: "\n"
  });
 
  for(let row of data) {
    await page.goto(`${config.server.drupalURL}${config.entity[row.entity].add_bundle}`)
    await page.type('.required.machine-name-source', row.label);
    await page.waitForSelector('[id$="-machine-name-suffix"]', {visible: true});
    await page.click('[id$="-machine-name-suffix"] > span.admin-link > button.link');
    await page.$eval(
      '.js-form-type-machine-name input',
      (element, row) => {
        element.value = row.bundle;
      },
      row
    );
    
    switch(row.entity) {
      case "node":
      break;
    }
    await page.click('#edit-actions [name="op"]');
    await page.waitForSelector('div[aria-label="Status message"]');
  }

  result = fs.readFileSync('./entity_fields.csv');
  data = await neatcsv(result, {
    separator: "\t",
    newline: "\n"
  });
  for(let row of data) {
    let fieldUIURL = config.entity[row.entity].field_ui;
    if (row.bundle) {
      fieldUIURL = fieldUIURL.replace('{bundle}', row.bundle);
    }
    await page.goto(`${config.server.drupalURL}${fieldUIURL}/add-field`);
    if(
      await page.$('#edit-existing-storage-name') !== null &&
      await page.$(`#edit-existing-storage-name > option[value="${row.field_name}"]`) !== null
    ) {
      await page.select('#edit-existing-storage-name', `${row.field_name}`);
      await page.waitForSelector('#edit-existing-storage-label', {visible: true});
      await page.$eval(
        '#edit-existing-storage-label',
        (element, row) => {
          element.value = row.field_label
        },
        row
      )
    }
    else {
      await page.select('#edit-new-storage-type', `${row.field_type}`);
      await page.type('#edit-label', `${row.field_label}`);
      await page.waitForSelector('#edit-label-machine-name-suffix', {visible: true});
      await page.click('#edit-label-machine-name-suffix button');
      await page.evaluate(
        (row) => {
          let fieldName = row.field_name;
          const prefix = document.querySelector('label[for="edit-field-name"]+span>span:first-child').textContent;
          document.querySelector('#edit-field-name').value = fieldName.replace(prefix, '');
        },
        row
      );
      await page.click('#edit-actions > #edit-submit')
      await page.waitForSelector('#edit-cardinality');

      // Field storage page.
      if (row.field_items === 'n') {
        await page.select('#edit-cardinality', '-1');
      }
      else {
        await page.$eval(
          '#edit-cardinality-number',
          (element, row) => {
            element.value = row.field_items;
          },
          row
        )
      }
    }

    // Field settings page.
    await page.click('#edit-actions > #edit-submit')
    await page.waitForSelector('#edit-required');

    if (row.required === '1') {
      await page.click('#edit-required');
    }
    await page.click('#edit-actions > #edit-submit')
  }
  await browser.close();
}

init().catch(
  (error) => {
    console.error(error);
  }
);